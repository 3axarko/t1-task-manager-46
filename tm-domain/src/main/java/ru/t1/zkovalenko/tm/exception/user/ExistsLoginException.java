package ru.t1.zkovalenko.tm.exception.user;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Login already exists");
    }

}
