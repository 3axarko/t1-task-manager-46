package ru.t1.zkovalenko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.model.IWBS;
import ru.t1.zkovalenko.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractUserOwnerModel implements IWBS {

    private static final long serialVersionUID = 1;

    @Nullable
    @ManyToOne
    private Project project;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(length = 2000)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    public Task(@NotNull String name) {
        this.name = name;
    }

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
