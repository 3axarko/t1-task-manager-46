package ru.t1.zkovalenko.tm.dto.response.user;

import lombok.NoArgsConstructor;
import ru.t1.zkovalenko.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractResponse {
}
