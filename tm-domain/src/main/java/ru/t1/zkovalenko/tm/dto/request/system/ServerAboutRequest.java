package ru.t1.zkovalenko.tm.dto.request.system;

import lombok.NoArgsConstructor;
import ru.t1.zkovalenko.tm.dto.request.AbstractRequest;

@NoArgsConstructor
public final class ServerAboutRequest extends AbstractRequest {
}
