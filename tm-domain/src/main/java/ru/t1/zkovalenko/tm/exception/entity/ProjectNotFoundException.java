package ru.t1.zkovalenko.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityException {

    public ProjectNotFoundException() {
        super("Project not Found");
    }

}
