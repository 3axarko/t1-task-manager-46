package ru.t1.zkovalenko.tm.api.repository.model;

import ru.t1.zkovalenko.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

}
