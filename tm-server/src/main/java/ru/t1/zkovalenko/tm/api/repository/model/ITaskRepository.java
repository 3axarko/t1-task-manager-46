package ru.t1.zkovalenko.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    @NotNull List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull List<Task> findAllByProjectId(@NotNull String projectId);

    void removeAllByProjectId(@NotNull String projectId);

}