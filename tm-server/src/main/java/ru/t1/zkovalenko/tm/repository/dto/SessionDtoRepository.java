package ru.t1.zkovalenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.zkovalenko.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

public final class SessionDtoRepository extends AbstractUserOwnerDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return this.getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

}
