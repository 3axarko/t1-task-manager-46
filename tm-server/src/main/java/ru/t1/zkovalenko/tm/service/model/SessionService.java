package ru.t1.zkovalenko.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.model.IUserOwnerRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.model.ISessionService;
import ru.t1.zkovalenko.tm.model.Session;
import ru.t1.zkovalenko.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public class SessionService extends AbstractUserOwnerService<Session> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull IUserOwnerRepository<Session> getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}
