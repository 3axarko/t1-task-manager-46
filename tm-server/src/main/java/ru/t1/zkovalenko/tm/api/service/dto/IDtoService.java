package ru.t1.zkovalenko.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.model.AbstractModelDTO;
import ru.t1.zkovalenko.tm.enumerated.Sort;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IDtoService<M extends AbstractModelDTO> {

    void clear();

    @NotNull List<M> findAll();

    @NotNull List<M> findAll(@Nullable Comparator comparator);

    @NotNull List<M> findAll(@Nullable Sort sort);

    @NotNull
    M add(M model);

    @NotNull
    M update(M model);

    @NotNull Collection<M> add(@NotNull Collection<M> models);

    @NotNull Collection<M> set(@NotNull Collection<M> models);

    boolean existById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    M remove(M model);

}
