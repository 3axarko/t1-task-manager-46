package ru.t1.zkovalenko.tm.api.service.model;

import ru.t1.zkovalenko.tm.model.Session;

public interface ISessionService extends IUserOwnerService<Session> {

}
