package ru.t1.zkovalenko.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnerDtoRepository<TaskDTO> {

    @NotNull TaskDTO create(@Nullable String userId,
                            @NotNull String name,
                            @Nullable String description);

    @NotNull TaskDTO create(@Nullable String userId, @NotNull String name);

    @NotNull List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String projectId);

}