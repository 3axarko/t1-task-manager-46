package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.model.SessionDTO;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable SessionDTO session);

}
