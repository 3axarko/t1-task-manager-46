package ru.t1.zkovalenko.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnerDtoRepository<ProjectDTO> {

    @NotNull ProjectDTO create(@Nullable String userId,
                               @NotNull String name,
                               @Nullable String description);

    @NotNull ProjectDTO create(@Nullable String userId, @NotNull String name);

}
