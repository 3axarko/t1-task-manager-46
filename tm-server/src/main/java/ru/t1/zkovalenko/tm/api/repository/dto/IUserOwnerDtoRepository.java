package ru.t1.zkovalenko.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.model.AbstractUserOwnerModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerDtoRepository<M extends AbstractUserOwnerModelDTO> extends IDtoRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    boolean existById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@Nullable String userId, @NotNull Integer index);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    int getSize(@NotNull String userId);

    @NotNull List<M> findAll(@Nullable String userId);

    @NotNull List<M> findAll(@Nullable String userId, @Nullable Comparator comparator);

    void clear(@Nullable String userId);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @NotNull Integer index);

}
