package ru.t1.zkovalenko.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.endpoint.IAuthEndpoint;
import ru.t1.zkovalenko.tm.api.service.IAuthService;
import ru.t1.zkovalenko.tm.api.service.IServiceLocator;
import ru.t1.zkovalenko.tm.dto.model.SessionDTO;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;
import ru.t1.zkovalenko.tm.dto.request.user.UserLoginRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserLogoutRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.zkovalenko.tm.dto.response.user.UserLoginResponse;
import ru.t1.zkovalenko.tm.dto.response.user.UserLogoutResponse;
import ru.t1.zkovalenko.tm.dto.response.user.UserViewProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.zkovalenko.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final String token = getAuthService().login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        getAuthService().logout(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final UserDTO user = getServiceLocator().getUserService().findOneById(userId);
        return new UserViewProfileResponse(user);
    }

}
