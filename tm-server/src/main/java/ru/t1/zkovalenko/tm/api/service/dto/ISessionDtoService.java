package ru.t1.zkovalenko.tm.api.service.dto;

import ru.t1.zkovalenko.tm.dto.model.SessionDTO;

public interface ISessionDtoService extends IUserOwnerDtoService<SessionDTO> {

}
