package ru.t1.zkovalenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.zkovalenko.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public final class ProjectDtoRepository extends AbstractUserOwnerDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return this.getClazz().getSimpleName();
    }

    @NotNull
    @Override
    protected Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

    @NotNull
    @Override
    public ProjectDTO create(@Nullable final String userId,
                             @NotNull final String name,
                             @Nullable final String description) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description == null ? "" : description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public ProjectDTO create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }


}