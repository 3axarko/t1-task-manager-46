package ru.t1.zkovalenko.tm.constant.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.model.Task;

public final class TaskTestData {

    @NotNull
    public final static String TASK_NAME = "TASK_NAME";

    @NotNull
    public final static String TASK_DESCRIPTION = "TASK_DESCRIPTION";

    @NotNull
    public final static Task TASK1 = new Task();

    @NotNull
    public final static Task TASK2 = new Task();

    static {
        TASK1.setName(TASK_NAME);
        TASK1.setDescription(TASK_DESCRIPTION);
    }

}
