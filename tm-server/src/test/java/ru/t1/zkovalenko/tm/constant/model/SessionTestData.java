package ru.t1.zkovalenko.tm.constant.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.model.Session;

import java.util.Date;

import static ru.t1.zkovalenko.tm.constant.model.UserTestData.USER1;

public final class SessionTestData {

    @NotNull
    public final static String SESSION_NAME = "SESSION_NAME";

    @NotNull
    public final static String SESSION_DESCRIPTION = "SESSION_DESCRIPTION";

    @NotNull
    public final static Session SESSION1 = new Session();

    @NotNull
    public final static Session SESSION2 = new Session();

    static {
        SESSION1.setUser(USER1);
        SESSION1.setDate(new Date());

    }

}
