package ru.t1.zkovalenko.tm.dtoService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.dto.IUserDtoService;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;
import ru.t1.zkovalenko.tm.exception.user.UserNotFoundException;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.service.ConnectionService;
import ru.t1.zkovalenko.tm.service.PropertyService;
import ru.t1.zkovalenko.tm.service.dto.UserDtoService;
import ru.t1.zkovalenko.tm.util.HashUtil;

import static ru.t1.zkovalenko.tm.constant.model.UserTestData.*;
import static ru.t1.zkovalenko.tm.enumerated.Role.ADMIN;

@Category({UnitCategory.class, UnitServiceCategory.class})
public final class UserDtoServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserDtoService userService = new UserDtoService(
            connectionService,
            propertyService
    );

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Nullable
    private UserDTO userCreated;

    @After
    public void after() {
        if (userCreated != null) userService.remove(userCreated);
        userCreated = null;
    }

    @Test
    public void createByLoginPassword() {
        userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        @Nullable final UserDTO userFunded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFunded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD));
        Assert.assertEquals(userFunded.getLogin(), USER1.getLogin());
    }

    @Test
    public void createByLoginPasswordEmail() {
        userCreated = userService.create(
                USER1.getLogin(),
                USER_PASSWORD,
                USER1.getEmail());
        @Nullable final UserDTO userFunded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFunded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD));
        Assert.assertEquals(userFunded.getLogin(), USER1.getLogin());
        Assert.assertEquals(userFunded.getEmail(), USER1.getEmail());
    }

    @Test
    public void createByLoginPasswordRule() {
        userCreated = userService.create(
                USER1.getLogin(),
                USER_PASSWORD,
                ADMIN);
        @Nullable final UserDTO userFunded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFunded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD));
        Assert.assertEquals(userFunded.getLogin(), USER1.getLogin());
        Assert.assertEquals(userFunded.getRole(), ADMIN);
    }

    @Test
    public void findByLogin() {
        userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        @NotNull final UserDTO userFounded = userService.findByLogin(USER1.getLogin());
        Assert.assertEquals(userCreated.getId(), userFounded.getId());
    }

    @Test
    public void findByEmail() {
        userCreated = userService.create(
                USER1.getLogin(),
                USER_PASSWORD,
                USER1.getEmail());
        @NotNull final UserDTO userFounded = userService.findByEmail(USER1.getEmail());
        Assert.assertEquals(userCreated.getId(), userFounded.getId());
    }

    @Test
    public void removeByLogin() {
        userService.create(USER1.getLogin(), USER_PASSWORD);
        userService.findByLogin(USER1.getLogin());
        userService.removeByLogin(USER1.getLogin());
        thrown.expect(UserNotFoundException.class);
        userService.findByLogin(USER1.getLogin());
    }

    @Test
    public void removeByEmail() {
        userCreated = userService.create(USER1.getLogin(), USER_PASSWORD, USER1.getEmail());
        userService.findByEmail(USER1.getEmail());
        userService.removeByLogin(USER1.getLogin());
        thrown.expect(UserNotFoundException.class);
        userService.findByLogin(USER1.getLogin());
    }

    @Test
    public void setPassword() {
        userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        userService.setPassword(userCreated.getLogin(), USER_PASSWORD + "1");
        @Nullable final UserDTO userFounded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFounded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD + "1"));
    }

    @Test
    public void updateUser() {
        userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        userService.updateUser(
                userCreated.getId(),
                TEST_FIO,
                TEST_FIO,
                TEST_FIO);
        @Nullable final UserDTO userFounded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFounded.getFirstName(), TEST_FIO);
        Assert.assertEquals(userFounded.getLastName(), TEST_FIO);
        Assert.assertEquals(userFounded.getMiddleName(), TEST_FIO);
    }

    @Test
    public void isLoginExist() {
        userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
    }

    @Test
    public void isEmailExist() {
        userCreated = userService.create(USER1.getLogin(), USER_PASSWORD, USER1.getEmail());
        Assert.assertTrue(userService.isEmailExist(USER1.getEmail()));
    }

    @Test
    public void lockUnlockUserByLogin() {
        userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        userService.lockUserByLogin(USER1.getLogin());
        @Nullable UserDTO userFounded = userService.findByLogin(USER1.getLogin());
        Assert.assertTrue(userFounded.getLocked());
        userService.unlockUserByLogin(USER1.getLogin());
        userFounded = userService.findByLogin(USER1.getLogin());
        Assert.assertFalse(userFounded.getLocked());
    }

}
