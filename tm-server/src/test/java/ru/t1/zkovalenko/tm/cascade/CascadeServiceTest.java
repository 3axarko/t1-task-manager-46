package ru.t1.zkovalenko.tm.cascade;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.model.IProjectService;
import ru.t1.zkovalenko.tm.api.service.model.ISessionService;
import ru.t1.zkovalenko.tm.api.service.model.ITaskService;
import ru.t1.zkovalenko.tm.api.service.model.IUserService;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.Session;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.service.ConnectionService;
import ru.t1.zkovalenko.tm.service.PropertyService;
import ru.t1.zkovalenko.tm.service.model.ProjectService;
import ru.t1.zkovalenko.tm.service.model.SessionService;
import ru.t1.zkovalenko.tm.service.model.TaskService;
import ru.t1.zkovalenko.tm.service.model.UserService;
import ru.t1.zkovalenko.tm.util.HashUtil;

import java.util.Arrays;
import java.util.List;

@Category(UnitServiceCategory.class)
public final class CascadeServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(
            connectionService,
            propertyService
    );

    @Before
    public void before() {
        try {
            userService.removeByLogin("login");
        } catch (Exception e) {
        }
    }

    @After
    public void after() {
        @NotNull final User userTest = userService.findByLogin("login");
        userService.remove(userTest);
    }

    @Test
    public void createUserWithProject() {
        final User userCreated = new User();
        userCreated.setLogin("login");
        userCreated.setPasswordHash(HashUtil.salt(propertyService, "password"));
        final List<Project> projectsCreated = Arrays.asList(new Project(), new Project());

        projectsCreated.forEach(project -> project.setUser(userCreated));
        userCreated.setProjects(projectsCreated);
        userService.add(userCreated);

        final List<Project> projectsFounded = projectService.findAll(userCreated.getId());
        Assert.assertTrue(projectsFounded.retainAll(projectsCreated));
    }

    @Test
    public void createUserWithTask() {
        final User userCreated = new User();
        userCreated.setLogin("login");
        userCreated.setPasswordHash(HashUtil.salt(propertyService, "password"));
        final List<Task> tasksCreated = Arrays.asList(new Task(), new Task());

        tasksCreated.forEach(task -> task.setUser(userCreated));
        userCreated.setTasks(tasksCreated);
        userService.add(userCreated);

        final List<Task> tasksFounded = taskService.findAll(userCreated.getId());
        Assert.assertTrue(tasksFounded.retainAll(tasksCreated));
    }

    @Test
    public void createUserWithSession() {
        final User userCreated = new User();
        userCreated.setLogin("login");
        userCreated.setPasswordHash(HashUtil.salt(propertyService, "password"));
        final List<Session> sessionsCreated = Arrays.asList(new Session(), new Session());

        sessionsCreated.forEach(session -> session.setUser(userCreated));
        userCreated.setSessions(sessionsCreated);
        userService.add(userCreated);

        final List<Session> sessionsFounded = sessionService.findAll(userCreated.getId());
        Assert.assertTrue(sessionsFounded.retainAll(sessionsCreated));
    }

}
