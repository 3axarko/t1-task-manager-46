package ru.t1.zkovalenko.tm.cascade;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.service.ConnectionService;
import ru.t1.zkovalenko.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.Arrays;

@Category(UnitServiceCategory.class)
public final class CascadeEMTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final EntityManager entityManager = connectionService.getEntityManager();

    @Before
    public void before() {
        entityManager.getTransaction().begin();
    }

    @After
    public void after() {
        if (entityManager.isOpen()) {
            entityManager.getTransaction().commit();
            entityManager.close();
        }
    }

    @Test
    public void insertRemoveCascade() {
        final User userCreated = new User();
        userCreated.setLogin(String.format("login%s", new Timestamp(System.currentTimeMillis())));
        userCreated.setPasswordHash("1");
        final Project projectCreated = new Project();
        final Task taskCreated = new Task();

        taskCreated.setProject(projectCreated);
        projectCreated.setTasks(Arrays.asList(taskCreated));
        projectCreated.setUser(userCreated);
        userCreated.setProjects(Arrays.asList(projectCreated));

        entityManager.persist(userCreated);
        entityManager.getTransaction().commit();
        entityManager.close();

        final EntityManager entityManager1 = connectionService.getEntityManager();
        try {
            final Project projectFounded = entityManager1.find(Project.class, projectCreated.getId());
            Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
            final Task taskFounded = entityManager1.find(Task.class, taskCreated.getId());
            Assert.assertEquals(taskCreated.getId(), taskFounded.getId());

            entityManager1.getTransaction().begin();
            entityManager1.remove(entityManager1.find(User.class, userCreated.getId()));
            Assert.assertNull(entityManager1.find(Task.class, taskCreated.getId()));
        } finally {
            entityManager1.close();
        }
    }

}
