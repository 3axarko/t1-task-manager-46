package ru.t1.zkovalenko.tm.constant.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;

import java.util.Arrays;
import java.util.List;

public final class UserDTOTestData {

    @NotNull
    public final static UserDTO USER1 = new UserDTO();

    @NotNull
    public final static UserDTO USER2 = new UserDTO();

    @NotNull
    public final static String USER_LOGIN = "USER_LOGIN";

    @NotNull
    public final static String USER_PASSWORD = "USER_PASSWORD";

    @NotNull
    public final static String USER_EMAIL = "12@mail.ru";

    @NotNull
    public final static String TEST_FIO = "TEST";

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER1, USER2);

    static {
        for (Integer i = 0; i < USER_LIST.size(); i++) {
            USER_LIST.get(i).setLogin(USER_LOGIN + i);
            USER_LIST.get(i).setEmail(i + USER_EMAIL);
        }
    }

}
