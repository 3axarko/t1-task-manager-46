package ru.t1.zkovalenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.zkovalenko.tm.api.endpoint.IAuthEndpoint;
import ru.t1.zkovalenko.tm.api.endpoint.IUserEndpoint;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;
import ru.t1.zkovalenko.tm.dto.request.user.*;
import ru.t1.zkovalenko.tm.exception.user.AccessDeniedException;
import ru.t1.zkovalenko.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.zkovalenko.tm.exception.user.UserNotFoundException;
import ru.t1.zkovalenko.tm.marker.IntegrationCategory;
import ru.t1.zkovalenko.tm.service.PropertyService;

import static ru.t1.zkovalenko.tm.constant.EndpointTestData.USER_ADMIN_LOGIN;
import static ru.t1.zkovalenko.tm.constant.EndpointTestData.USER_ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class UserEndpointTest extends AbstractEndpointTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Nullable
    private String userToken;

    @NotNull
    private UserDTO testUser;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String USER_LOGIN = "USER_LOGIN";

    @NotNull
    private final String USER_PASSWORD = "USER_PASSWORD";

    @NotNull
    private final String USER_EMAIL = "USER_EMAIL@mail.ru";

    @NotNull
    private final String USER_FIRST_NAME = "USER_FIRST_NAME";

    @NotNull
    private final String USER_LAST_NAME = "USER_FIRST_NAME";

    @NotNull
    private final String USER_MIDDLE_NAME = "USER_MIDDLE_NAME";

    @NotNull
    private final String ERROR_LOGIN_PASSWORD = new IncorrectLoginOrPasswordException().getMessage();

    @NotNull
    private final String ERROR_USER_NOT_FOUND = new UserNotFoundException().getMessage();

    @NotNull
    private final String ERROR_NOT_LOGIN = new AccessDeniedException().getMessage();

    @NotNull
    private IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private UserDTO testUserCreate() {
        @NotNull final UserRegistryRequest createRequest = new UserRegistryRequest(userToken);
        createRequest.setLogin(USER_LOGIN);
        createRequest.setPassword(USER_PASSWORD);
        createRequest.setEmail(USER_EMAIL);
        @Nullable UserDTO userCreated = userEndpoint.registryUser(createRequest).getUser();
        Assert.assertNotNull(userCreated);
        return userCreated;
    }

    @Before
    public void before() {
        userToken = login(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD);
        saveBackup(userToken);
        serverAutoBackup(false, userToken);
        testUser = testUserCreate();
    }

    @After
    public void after() {
        userToken = login(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD);
        loadBackup(userToken);
        serverAutoBackup(true, userToken);
        logout(userToken);
    }

    @Test
    public void userChangePassword() {
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(userToken);
        request.setLogin(USER_LOGIN);
        request.setPassword(USER_PASSWORD + "1");
        userEndpoint.changeUserPassword(request);
        thrown.expectMessage(ERROR_LOGIN_PASSWORD);
        login(USER_LOGIN, USER_PASSWORD);
    }

    @Test
    public void userLock() {
        @NotNull final UserLockRequest request = new UserLockRequest(userToken);
        request.setLogin(USER_LOGIN);
        userEndpoint.lockUser(request);
        thrown.expectMessage(ERROR_LOGIN_PASSWORD);
        login(USER_LOGIN, USER_PASSWORD);
    }

    @Test
    public void userUnlock() {
        @NotNull final UserLockRequest request = new UserLockRequest(userToken);
        request.setLogin(USER_LOGIN);
        userEndpoint.lockUser(request);
        @NotNull final UserUnlockRequest requestUnlock = new UserUnlockRequest(userToken);
        requestUnlock.setLogin(USER_LOGIN);
        userEndpoint.unlockUser(requestUnlock);
        login(USER_LOGIN, USER_PASSWORD);
    }

    @Test
    public void userRemove() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(userToken);
        request.setLogin(USER_LOGIN);
        userEndpoint.removeUser(request);
        thrown.expectMessage(ERROR_USER_NOT_FOUND);
        login(USER_LOGIN, USER_PASSWORD);
    }

    @Test
    public void userUpdateProfile() {
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(userToken);
        request.setFirstName(USER_FIRST_NAME);
        request.setLastName(USER_LAST_NAME);
        request.setMiddleName(USER_MIDDLE_NAME);
        userEndpoint.updateProfileUser(request);
        @NotNull final UserViewProfileRequest requestProfile = new UserViewProfileRequest(userToken);
        @Nullable final UserDTO userFounded = authEndpoint.profile(requestProfile).getUser();
        Assert.assertEquals(USER_FIRST_NAME, userFounded.getFirstName());
        Assert.assertEquals(USER_LAST_NAME, userFounded.getLastName());
        Assert.assertEquals(USER_MIDDLE_NAME, userFounded.getMiddleName());
    }

    @Test
    public void userLogout() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(userToken);
        request.setToken(userToken);
        @NotNull final UserViewProfileRequest requestProfile = new UserViewProfileRequest(userToken);
        authEndpoint.profile(requestProfile);
        authEndpoint.logout(request);
        thrown.expectMessage(ERROR_NOT_LOGIN);
        authEndpoint.profile(requestProfile);
    }

    @Test
    public void userProfile() {
        @NotNull final String testToken = login(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(testToken);
        @Nullable final UserDTO user = authEndpoint.profile(request).getUser();
        Assert.assertEquals(USER_LOGIN, user.getLogin());
        Assert.assertEquals(USER_EMAIL, user.getEmail());
    }

}
