package ru.t1.zkovalenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;
import ru.t1.zkovalenko.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-view-profile";

    @NotNull
    private final String DESCRIPTION = "User view profile";

    @Override
    public void execute() {
        System.out.println("[USER VIEW PROFILE]");
        UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @NotNull final UserDTO user = getAuthEndpoint().profile(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
